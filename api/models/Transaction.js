/**
* Transaction.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	attributes: {
		date: {
			required: true,
			type: 'date'
		},
		ref: {
			required: true,
			type: 'string'
		},
		amount: {
			required: true,
			type: 'decimal',
		},
		remarks: {
			required: true,
			type: 'string'
		},
		flow: {
			required: true,
			type: 'string'
		},
		value_date: {
			required: true,
			type: 'date'
		},
		balance: {
			required: true,
			type: 'decimal'
		}
	},
	validationMessages: {
		date: {
			required: 'A valid date is required',
			type: 'Provided date is not valid'
		},
		ref: {
			required: 'Transaction reference is required'
		},
		amount: {
			required: 'A valid amount is required',
			type: 'Provided amount is not decimal'
		},
		remarks: {
			required: 'A remark is required'
		},
		flow: {
			required: 'A transaction type is required',
		},
		value_date: {
			required: 'A valid date is required',
			type: 'Provided date is not valid'
		},
		balance: {
			type: 'Provided balance is not decimal'
		}
	}
};