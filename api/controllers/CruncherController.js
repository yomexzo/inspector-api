/**
 * CruncherController
 *
 * @description :: Server-side logic for managing files
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var GTB = 'gtbank',
	uploadFile = function(fileObj, successCb, errorCb){
		fileObj.upload(function (err, files) {
			if (err){
				errorCb(err);
			}else if(files){
				successCb(files);
			}else{
				res.send(500, {response: {message: 'An unkown error has occured'}})
			}
		});
	},
	gtbCruncher = function(records, cb){
		// var income = [];
		// var expenses = [];
		var transactions = [];
		records.forEach(function(record, index){
			if(Date.parse(record[0]) > 0 && Date.parse(record[2]) > 0){
				transactions.push({
					date: new Date(record[0]).toString(),
					ref: record[1],
					amount: isNaN(parseFloat(record[3])) ? record[4] : record[3],
					remarks: record[6],
					flow: isNaN(parseFloat(record[3])) ? 'income' : 'expenses',
					value_date: new Date(record[2]).toString(),
					balance: record[5]
				});
			}
		});
		cb(transactions);
	};

module.exports = {
	crunch: function(req, res) {

		var crunchCb = function(transactions){
			Transaction.create(transactions).exec(function (err, transactions){
				if(err){
					return res.negotiate(err);
				}else if(transactions){
					return res.send(200, transactions);
				}else{
					return res.send(500, {response: {message: 'An unknown error occured'}});
				}
			})
		}

		var crunch = function(records){
			var bank = req.param('bank') || GTB;
			switch (bank) {
				case GTB:
					gtbCruncher(records, crunchCb);
					break;
				default:
					res.send(400, {response: {
						message: 'Input error', 
						errors: {bank: 'Bank provided is not valid'}
					}})
					break;
			}
		}
		
		var errorUploading = function(err){
			res.serverError(err);
		};

		var uploadCb = function(files){


	        // var XLSX = require('xlsx');
	        // var workbook = XLSX.readFile(files[0]['fd']);
			// gtbCruncher(workbook.Sheets[0]);
			var excelParser = require('excel-parser');
			excelParser.parse({ 
				inFile: files[0]['fd'],
				worksheet: 1,
				skipEmpty: false,
			}, function(err, records){
				if(err) console.error(err);
				var fs = require('fs');
				files.forEach(function (file){
					fs.unlinkSync(file['fd']);
				});
				crunch(records);
			});
		};

		uploadFile(req.file('statement'), uploadCb, errorUploading);
		

		
	},

	transactions: function(req, res){
		var msg = {
			response: {
				message: 'Organisations retrieved successfully.'
			}
		}
		// var limit = req.param('limit') || 10;
		// var page = req.param('page') || 1;
		// .paginate({page: page, limit: limit})
		Transaction.native(function(err, collection){
			if(err){
				res.negotiate(err);
			} else{
				var mapFunc = function() {
					var payLoad = {
						date: this.date,
						ref: this.ref,
						amount: this.amount,
						remarks: this.remarks
					}
					payLoad.meta = {
						type: 'other',
						from: 'unkown',
						to: 'unkown'
					}
					var transfer = payLoad.remarks.match(/Transfer/gi);
					var pos = payLoad.remarks.match(/pos/gi);
					var atm = payLoad.remarks.match(/atm/gi);
					var airtime = payLoad.remarks.match(/airtime/gi);
					payLoad.meta.type = payLoad.remarks.match(/transfer/gi);
					if(transfer){
						payLoad.meta.type = 'Transfer';
					}else if(pos){
						payLoad.meta.type = 'POS';
					}else if(atm){
						payLoad.meta.type = 'ATM';
					}else if(airtime){
						payLoad.meta.type = 'Airtime';
					}
					emit(this['flow'], {set: [payLoad]});
					emit(this['flow']+'_total', parseFloat(this['amount']));
				};
				var reduceFunc = function(key, obj) {
					if(key == 'income_total' || key == 'expenses_total'){
						var total = 0;
						for(k in obj){
							total += obj[k];
						}
						return total;
					}else if(key == 'income' || key =='expenses'){
						var resp = {set: []};
						for(k in obj){
							for(y in obj[k]['set'])
								resp.set.push(obj[k]['set'][y]);
						}
						return resp;
					}else{
						return {};
					}
				};
				var finalizeFunc = function (key, reducedVal) {
					if(key == 'income' || key == 'expenses'){
						return reducedVal.set;
					}
					return reducedVal;
				};
				var cb = function (err, results) {
					if(err) res.negotiate(err);
					var response = {};
					results.forEach(function(curr){
						response[curr['_id']] = curr['value'];
					});
					// response = results;
					return res.send(200, response);
				};
						// query: { ord_date: { $gt: new Date('01/01/2012') } },
				collection.mapReduce(mapFunc, reduceFunc,
					{ 
						out: { inline: 1 }, 
						finalize: finalizeFunc 
					}, cb
				);
			}
		})
	}
	// { 
	// 	"time": "September 2014",
	// 	"opening_balance": "20000",
	// 	"expense_total": "240000", 
	// 	"income_total": "300000", 
	// 	"transactions": "POS, ATM, TRANSFER, AIRTIME"
	// }
};

